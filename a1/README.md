# LIS 3781

## John Corrigan

### Assignment #1 Requirements:

*Sub-Heading:*

1. Initialize class repo
2. Install AMPPS
3. Provide git command descriptions

#### README.md file should include the following items:

* Screenshot of A1 erd
* Ex1. SQL solution
* Git commands w/short descriptions


> #### Git commands w/short descriptions:

1. git init - create new local repository
2. git status - gets status of current git repo
3. git add -  adds the file to index
4. git commit - commits changes
5. git push - push from local directory to remote server
6. git pull - pulls changes from remote to local
7. git diff - differences between local directory and stage

#### Assignment Screenshots:

*Screenshot of A1 ERD:

![A1 erd Screenshot](img/a1_erd_screenshot.png)

*Screenshot of SQL query and result*:

![SQL query example and result](img/sql_example_screenshot.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")