# LIS 3781

## John Corrigan

### Assignment #5 Requirements:

1. Create tables according to business rules
2. Populate tables on MS SQL Server
3. Create ERD model of tables and relationships

#### README.md file should include the following items:

* Screenshot of ERD in MS SQL SERVER
* populated tables
* Stored procedure example


#### Assignment Screenshots:

*Screenshot of A5 ERD full view:*

![A5 erd Screenshot](img/erd-full.png)

*Screenshot zoomed in on erd*:

![A4 erd Screenshot](img/zoomed-1.png)
![A4 erd Screenshot](img/zoomed-2.png)


*Stored procedure SQL and result:*

<table><tr>
<td> <img src="img/stored-procedure.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/result.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>