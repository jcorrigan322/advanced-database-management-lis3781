# LIS 3781

## John Corrigan

### Assignment #2 Requirements:

1. Create and populate customer and company tables
2. Populate tables locally and cci server
3. Create user1 and user2 locally with specefic grants

#### README.md file should include the following items:

* Screenshot of sql code
* Screenshots of populated tables


#### Assignment Screenshots:

*Screenshot of SQL Code http://localhost*:

![SQL Code](img/sql_code1.png)
![SQL code](img/sql_code2.png)

*Screenshot of localy populated tables*:

![Tables populated localy](img/tables_local.png)

*Screenshot of user1 logged in and grants*:

![user1 logged in](img/logged_in_user1.png)
![grants user1](img/user1_grants.png)

*Screenshot of user2 logged in and grants*:

![user2 logged in](img/logged_in_user2.png)
![grants user2](img/user2_grants.png)

*Screenshot of Data structures as admin*:

![admin data structures](img/data_structures_admin.png)

*Screenshot of ERD*:

![a2 erd](img/a2_erd.png)