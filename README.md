# LIS 3781

## John Corrigan

### Class 3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Instal AMPPS
    - Create bitbucket repo
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create customer and company tables
    - populate tables localy and on cci server
    - create user1 and user2 with specified grants

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create customer, order, and commodity tables
    - populate tables on Oracle server
    - Oracle Select Statements

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create tables and erd
    - populate tables on cci server
    - various select statements

5. [A4 README.MD](a4/README.md "My A4 README.md file")
    - Create tables per business rules
    - populate tables on MS SQL Server
    - Create erd of tables and reltationships

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create tables per business rules
    - populate tables on MS SQL Server
    - Create erd of tables and relationships

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Install and configure Mongodb
    - Use primer dataset locally in Mongodb
    - Various select statements and crud operations