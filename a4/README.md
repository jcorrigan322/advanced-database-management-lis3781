# LIS 3781

## John Corrigan

### Assignment #4 Requirements:

1. Create tables according to business rules
2. Populate tables on MS SQL Server
3. Create ERD model of tables and relationships

#### README.md file should include the following items:

* Screenshot of ERD in MS SQL SERVER
* Populated person table


#### Assignment Screenshots:

*Screenshot of A4 ERD full view:*

![A4 erd Screenshot](img/erd1.png)

*Screenshot zoomed in on erd*:



![A4 erd Screenshot](img/erd2.png)
![A4 erd Screenshot](img/erd3.png)