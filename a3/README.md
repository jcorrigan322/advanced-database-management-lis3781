# LIS 3781

## John Corrigan

### Assignment #3 Requirements:

1. Create and populate customer and commodity and order tables
2. Populate tables oracle server
3. Various select statements

#### README.md file should include the following items:

* Screenshot of sql code
* Screenshots of populated tables
* file links


#### Assignment Screenshots:

*Screenshot of SQL Code*:

<table><tr>
<td> <img src="img/my_code1.png" alt="Drawing" style="width: 250px;" /> </td>
<td> <img src="img/my_code2.png" alt="Drawing" style="width: 250px;" style="height: 400px;"/> </td>
</tr></table>

![SQL code](img/my_code3.png)

*Screenshot of populated tables in oracle server*:

![Customer table](img/customer_ss.png)

<table><tr>
<td> <img src="img/commodity_ss.png" alt="Drawing" style="width: 250px;"/> </td>
<td> <img src="img/order_ss.png" alt="Drawing" style="width: 250px;"/> </td>
</tr></table>
