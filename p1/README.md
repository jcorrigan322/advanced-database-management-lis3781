# LIS 3781

## John Corrigan

### Project #1 Requirements:

1. Create erd and tables
2. Populate tables on cci server
3. Various select statements

#### README.md file should include the following items:

* Screenshot of erd
* Screenshots of populated person table
* file links


#### Assignment Screenshots:
*Screenshot of ERD*:

![ERD](img/erd_ss.png)

*Screenshot of populated person table on workbench*:

![Person table](img/person_table_workbench.png)

*Screenshot of populated person table in terminal*:

![Person table](img/person_table_terminal.png)
