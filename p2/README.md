# LIS 3781

## John Corrigan

### Project #2 Requirements:

1. Install and configure Mongodb
2. Use primer dataset locally in Mongodb
3. Various select statements and crud operations

#### README.md file should include the following items:

* Screenshot of collection restaurants
* Screenshots of MongoDB shell commands
* Screenshot of report commands and result


#### Assignment Screenshots:
*Screenshot of find one command*:

![Find One](img/find-one.png)

*Screenshot of find count command*:

![Find count](img/find-count.png)

*Screenshot of inserting a record example*:

![Insert record](img/insert-record.png)

*Screenshot of inserting a record verification*:

![Insert verify](img/verify-inserted.png)
